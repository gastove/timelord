extern crate clap;
extern crate reqwest;
extern crate serde;
extern crate serde_json;

mod http;
mod timedatectl;

use clap::{App, Arg};
use geoclue;
use timedatectl as tctl;

fn main() {
    let matches = App::new("Timelord")
        .version("0.1.0")
        .author("Ross Donaldson")
        .about("Works out a location from wifi and updates your system timezone accordingly.")
        .arg(
            Arg::with_name("check")
                .short("c")
                .long("check")
                .help("Fetch a timezone, but don't change system configs"),
        )
        .get_matches();

    let check = matches.is_present("check");

    println!("Trying to determine correct timezone...");
    let clue = geoclue::get_a_clue();
    let loc = clue.get_location();
    match http::get_timezone(loc.latitude, loc.longitude) {
        Ok(tz) => {
            if !check {
                println!("Setting system timezone to: {}", tz);
                let output = tctl::set_timezone(tz);
                println!("{}", output);
            } else {
                println!("I think your timezone is: {}", tz);
            }
        }
        Err(e) => println!("Faaaaail: {}", e),
    }
}
