use std::collections::HashMap;
use std::convert::From;
use std::{env, fmt};
use std::error::Error;

use reqwest as r;
use serde_json as json;

const API_URL: &str = "http://api.timezonedb.com/v2.1/get-time-zone";
const API_KEY_VAR: &str = "TIMEZONEDB_KEY";

mod timezonedb {

    use serde::Deserialize;

    #[derive(Debug, Deserialize, Eq, PartialEq)]
    pub enum QueryStatus {
        #[serde(rename = "OK")]
        Ok,
        #[serde(rename = "FAILED")]
        Failed,
    }

    #[derive(Debug, Deserialize)]
    pub struct ApiResponse {
        pub status: QueryStatus,
        pub message: Option<String>,
        #[serde(rename = "countryCode")]
        pub country_code: String,
        #[serde(rename = "countryName")]
        pub country_name: String,
        #[serde(rename = "zoneName")]
        pub zone_name: String,
        pub abbreviation: String,
        #[serde(rename = "nextAbbreviation")]
        pub next_abbreviation: String,
        #[serde(rename = "gmtOffset")]
        pub gmt_offset: i64,
        dst: String,
        #[serde(rename = "zoneStart")]
        pub zone_start: i64,
        #[serde(rename = "zoneEnd")]
        pub zone_end: i64,
        pub timestamp: i64,
        pub formatted: String,
        #[serde(rename = "totalPage")]
        pub total_page: Option<i32>,
        #[serde(rename = "currentPage")]
        pub current_page: Option<i32>,
    }

    #[cfg(test)]
    mod test {

        use super::*;

        #[test]
        fn test_deserialization() {
            let stunt_json = r#"
{
    "status":"OK",
    "countryCode":"US",
    "countryName":"United States",
    "zoneName":"America\/Chicago",
    "abbreviation":"CST",
    "gmtOffset":-21600,
    "dst":"0",
    "zoneStart":1446361200,
    "zoneEnd":1457856000,
    "nextAbbreviation":"CDT",
    "timestamp":1454446991,
    "formatted":"2016-02-02 21:03:11"
}
"#;
            let parsed: ApiResponse = json::from_str(&stunt_json).expect("Faled to parse");
            assert_eq!(parsed.status, QueryStatus::Ok);
            assert_eq!(parsed.message, None);
            assert_eq!(parsed.dst, "0");
        }
    }
}

#[derive(Debug)]
enum FailureCase {
    ApiKeyUnset,
    ApiKeyInvalidUnicode,
    ApiError(String),
}

#[derive(Debug)]
pub struct TimelordError {
    cause: FailureCase
}

impl fmt::Display for TimelordError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TimelordError, caused by: {}", self.description())
    }
}


impl Error for TimelordError {
    fn description(&self) -> &str {
        match &self.cause {
            FailureCase::ApiKeyUnset => "API Key not found in environment",
            FailureCase::ApiKeyInvalidUnicode => "API Key was set, but is invalid as unicode",
            FailureCase::ApiError(e) => &e,
        }
    }

    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl From<env::VarError> for TimelordError {
    fn from(err: env::VarError) -> TimelordError {
        let cause = match err {
            env::VarError::NotPresent => FailureCase::ApiKeyUnset,
            env::VarError::NotUnicode(_) => FailureCase::ApiKeyInvalidUnicode,
        };

        TimelordError{cause}
    }
}

impl From<r::Error> for TimelordError {
    fn from(err: r::Error) -> TimelordError {
        let desc = err.description();
        let cause = FailureCase::ApiError(desc.to_owned());

        TimelordError{cause}
    }
}

pub type Result<T> = std::result::Result<T, TimelordError>;

fn base_params() -> Result<HashMap<String, String>> {
    let api_key = env::var(API_KEY_VAR)?;
    let mut params = HashMap::new();
    params.insert(String::from("key"), api_key.to_owned());
    params.insert(String::from("format"), String::from("json"));
    params.insert(String::from("by"), String::from("position"));
    Ok(params)
}

pub fn get_timezone(lat: f64, long: f64) -> Result<String> {
    let mut params = base_params()?;
    params.insert(String::from("lat"), lat.to_string());
    params.insert(String::from("lng"), long.to_string());

    let client = r::Client::new();
    let mut resp = client.get(API_URL).query(&params).send()?;

    let text = resp.text().unwrap();

    if resp.status().is_success() {
        let parsed: timezonedb::ApiResponse = json::from_str(&text).unwrap();
        Ok(parsed.zone_name)
    } else {
        let cause = FailureCase::ApiError(text);
        Err(TimelordError{cause})
    }
}
