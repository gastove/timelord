use std::io::Result;
use std::process::{Command, Output};

pub fn set_timezone(zone: String) -> String {
    let result = Command::new("timedatectl")
        .arg("set-timezone")
        .arg(zone)
        .output();

    parse_output(result)
}

fn parse_output(res: Result<Output>) -> String {
    match res {
        Ok(output) => {
            if output.status.success() {
                String::from("Timezone updated 👍")
            } else {
                let stat = output.status.code().unwrap();
                format!("timedatectl set-timezone failed with status code {}", stat)
            }
        }
        Err(e) => format!("Executing timedatectl failed with:\n\t{}", e),
    }
}
