mod ffi {
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(dead_code)]
    #![allow(improper_ctypes)]

    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

use std::ffi::CString;

struct FFIString(String);

impl FFIString {
    pub fn as_ptr(self) -> *const i8 {
        let cstr = CString::new(self.0).unwrap();
        cstr.as_ptr()
    }
}

pub struct Location {
    pub latitude: f64,
    pub longitude: f64,
}

pub struct GeoClue {
    g_clue: *mut ffi::GClueSimple,
}

impl GeoClue {
    pub fn get_location(&self) -> Location {
        unsafe {
            let location = ffi::gclue_simple_get_location(self.g_clue);
            let latitude = ffi::gclue_location_get_latitude(location);
            let longitude = ffi::gclue_location_get_longitude(location);
            Location {
                latitude,
                longitude,
            }
        }
    }
}

pub fn get_a_clue() -> GeoClue {
    let desktop_id = FFIString(String::from("timelord"));
    let accuracy_level = ffi::GClueAccuracyLevel_GCLUE_ACCURACY_LEVEL_CITY;

    unsafe {
        let mut cancellable = ffi::g_cancellable_new();

        let g_clue = ffi::gclue_simple_new_sync(
            desktop_id.as_ptr(),
            accuracy_level,
            cancellable as *mut ffi::GCancellable,
            std::ptr::null_mut(),
        );
        GeoClue { g_clue }
    }
}

pub fn demo() {
    let clue = get_a_clue();
    let loc = clue.get_location();
    println!("Currently:\n\t- Lat: {}\n\t- Long: {}", loc.latitude, loc.longitude);
}
