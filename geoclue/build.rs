extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;

fn main() {

    let libs = pkg_config::Config::new()
        .atleast_version("2.5.3")
        .probe("libgeoclue-2.0")
        .unwrap();

    // pkg_config probes with both --libs and --cflags, but only emits
    // directives for libs. So, we get to do our inclusions ourselves.
    let mut inclusions = Vec::new();

    for lib in libs.include_paths {
        let incl = format!("-I{}", lib.as_path().display());
        inclusions.push(incl);
    }

    let bind_builder = bindgen::Builder::default().header("wrapper.h");

    let bindings = bind_builder
        .clang_args(inclusions)
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
